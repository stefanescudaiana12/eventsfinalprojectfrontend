import { Component } from '@angular/core';
import { Event } from "../model/event";
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-event-page',
  templateUrl: './event-page.component.html',
  styleUrls: ['./event-page.component.css']
})
export class EventPageComponent {
  event: Event = new Event(1,
    "Zilele orasului",
    "Description Event",
    "Iasi",
    new Date(2023, 7, 26, 10, 0, 0),
    new Date(2023, 7, 27, 12, 59, 0));

  httpClient: HttpClient;
  route: ActivatedRoute;
  constructor(httpclient: HttpClient, route: ActivatedRoute, private router: Router) {
    this.httpClient = httpclient;
    this.route = route;
  }

  ngOnInit(){
    const eventId = this.route.snapshot.paramMap.get("id");
    this.httpClient.get("/api/events/" + eventId).subscribe((response)=> {
      console.log(response);
      this.event = response as Event;
      this.event.startDate = new Date(this.event.startDate);
      this.event.endDate = new Date(this.event.endDate);
    },
      (error) => {
      console.log(error);
      if(error.error == "There is not event wit ID" + eventId){
        this.router.navigate(["/not-found"])
      }
    });
  }
}
